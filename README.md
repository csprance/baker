# xBaker #
## From and Scripts to Bake your maps in xNormal from modo ##

Installation is not configured yet as this is still a WIP kit.

### xBaker is a set of Scripts and forms that will allow you change and bake maps using xnormal from modo. ###

xNormal will accept a fiel full of bake settings and bake maps based on the settings in the file. The scripts create this xml file and the form allows you to store and change these values. Each time you hit bake with xNormal (or bind it to a hotkey) it will regenerate the xml file based on the settings in the form. I got this idea from backstube by Airborne Studios.

I want to expose all of the options within xnormal to the form. Why not. Strangely enough (and this may just be me I haven't tested) but I feel like I'm getting faster dialation times (Which was usually the longest part of the bake) So hooray!

* Works in mod 801 for sure 
* Version 0.01


### Please let me know if you have any interest in helping with this project I'd love to see any code ideas or suggestions ###

### Who do I talk to? ###

* Chris Sprance chrissprance@gmail.com
* http://www.csprance.com